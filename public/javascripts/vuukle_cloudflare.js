CloudFlare.define(
	'vuukle',
	['vuukle/config'],
	function(config){
		var orientation = 'right';
        	if( typeof config["comments-location"] !== 'undefined'){
                   orientation = config["comments-location"];
		}

		var url = 'https://vuukle.com/js/vuukleCF.js?orientation=' + orientation;		
		// Include vuukleCF.js on the page
		CloudFlare.require([url]);
	}
);
